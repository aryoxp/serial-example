﻿namespace SerialTest
{
    partial class SerialTestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label1;
            this.sPort = new System.IO.Ports.SerialPort(this.components);
            this.cbPorts = new System.Windows.Forms.ComboBox();
            this.btReloadPorts = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.btExec = new System.Windows.Forms.Button();
            label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(207, 17);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(56, 13);
            label1.TabIndex = 2;
            label1.Text = "Serial Port";
            // 
            // cbPorts
            // 
            this.cbPorts.FormattingEnabled = true;
            this.cbPorts.Location = new System.Drawing.Point(269, 13);
            this.cbPorts.Name = "cbPorts";
            this.cbPorts.Size = new System.Drawing.Size(121, 21);
            this.cbPorts.TabIndex = 1;
            // 
            // btReloadPorts
            // 
            this.btReloadPorts.Location = new System.Drawing.Point(396, 12);
            this.btReloadPorts.Name = "btReloadPorts";
            this.btReloadPorts.Size = new System.Drawing.Size(75, 23);
            this.btReloadPorts.TabIndex = 3;
            this.btReloadPorts.Text = "Reload";
            this.btReloadPorts.UseVisualStyleBackColor = true;
            this.btReloadPorts.Click += new System.EventHandler(this.btReloadPorts_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictureBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 42);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(459, 273);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Visualization";
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(71, 21);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(320, 240);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            // 
            // btExec
            // 
            this.btExec.Image = global::SerialTest.Properties.Resources.indicatorred;
            this.btExec.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btExec.Location = new System.Drawing.Point(200, 321);
            this.btExec.Name = "btExec";
            this.btExec.Padding = new System.Windows.Forms.Padding(0, 3, 0, 8);
            this.btExec.Size = new System.Drawing.Size(85, 47);
            this.btExec.TabIndex = 0;
            this.btExec.Text = "Start";
            this.btExec.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btExec.UseVisualStyleBackColor = true;
            this.btExec.Click += new System.EventHandler(this.btExec_Click);
            // 
            // SerialTestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(483, 380);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btReloadPorts);
            this.Controls.Add(label1);
            this.Controls.Add(this.cbPorts);
            this.Controls.Add(this.btExec);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "SerialTestForm";
            this.ShowIcon = false;
            this.Text = "Serial Port Image Communication Test";
            this.Load += new System.EventHandler(this.SerialTestForm_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.IO.Ports.SerialPort sPort;
        private System.Windows.Forms.Button btExec;
        private System.Windows.Forms.ComboBox cbPorts;
        private System.Windows.Forms.Button btReloadPorts;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox;
    }
}

