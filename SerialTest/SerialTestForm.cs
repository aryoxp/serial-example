﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;

namespace SerialTest
{
    public partial class SerialTestForm : Form
    {
        List<String> Ports;
        public SerialTestForm()
        {
            InitializeComponent();
            this.Ports = new List<String>();
            this.ReloadPortList();
        }

        private void ReloadPortList()
        {
            this.cbPorts.Text = "Loading...";
            this.Ports = System.IO.Ports.SerialPort.GetPortNames().ToList<String>();
            this.cbPorts.Items.Clear();
            this.cbPorts.Items.AddRange(this.Ports.ToArray());
            if (this.cbPorts.Items.Count == 0)
                this.cbPorts.Text = "-";
            else this.cbPorts.SelectedIndex = 0;
        }

        private void btExec_Click(object sender, EventArgs e)
        {
            if (this.btExec.Text == "Start")
            {
                Bitmap b = new Bitmap(320, 240, PixelFormat.Format16bppRgb565);
                this.pictureBox.Image = b;
                this.btExec.Image = SerialTest.Properties.Resources.indicatorgreen;
                this.btExec.Text = "Stop";
            }
            else
            {
                this.pictureBox.Image = null;
                this.btExec.Image = SerialTest.Properties.Resources.indicatorred;
                this.btExec.Text = "Start";
            }

        }

        private void btReloadPorts_Click(object sender, EventArgs e)
        {
            this.ReloadPortList();
        }

        private void SerialTestForm_Load(object sender, EventArgs e)
        {

        }
    }
}
